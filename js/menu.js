'use strict'

// DEFINED GLOBAL CONSTANTS

const menu_button = document.querySelector('.category-button');
const menu = document.querySelector('.menu');
const submenu = document.querySelectorAll('.submenu');
const materialName = document.querySelector('.material-name strong');
const materialDescription = document.querySelector('.tooltip-text')
const materialLink = document.querySelector('.material-link');
const available_categories =
    ['medicine',
        'minerals',
        'metals',
        'armory',
        'organics',
        'building',
        'animals',
        'fabrics',
        'electronics',
        'engineering',];

onPageLoad();

function onPageLoad() {
    let local_name = localStorage.getItem('name');
    materialLink.href = "https://yandex.ru/search/?text=" + local_name;
    materialName.innerHTML = local_name;
    materialDescription.innerHTML = localStorage.getItem('description of ' + local_name)

    document.addEventListener('DOMContentLoaded', changeDataElements, false);

    for (let item of submenu) {
        item.addEventListener('click', onSelectCategory);
    }
}

function showCategories() {
    for (let item of submenu) {
        item.classList.remove('hidden');
    }
}

function hideCategories() {
    for (let item of submenu) {
        item.classList.add('hidden');
    }
}

function showHideMenu() {
    if (menu.classList.contains('hidden')) { // В случае, если меню скрыто
        menu.classList.remove('hidden');
        menu_button.innerHTML = "Закрыть меню";
        showCategories();

    } else { // В ином случае
        menu.classList.add('hidden');
        menu.style.display = 'flex';
        menu_button.innerHTML = "Выбрать материал";
        menu.removeEventListener('click', selectEntry);

        for (let row of document.querySelectorAll('p.row')) {
            row.remove();
        }
    }
}

async function fetchMaterialsList(category) {
    let response = await fetch('php/MaterialList.php', {
        method: 'POST', body: category,
    });

    if (response.ok) {

        let text = await response.json();
        if (text.length === 0) {
            let row = document.createElement('p');
            row.className = 'row-text row';
            row.innerHTML = 'В выбранной категории нет материалов';
            row.addEventListener('click', function () {
                this.remove();
                showCategories();
            })
            menu.append(row);
            return 0;
        }
        menu.style.display = 'block';
        menu.addEventListener('click', selectEntry, false);

        printMaterialRows(text);

    } else {
        alert("Ошибка HTTP: " + response.status);
        showCategories();
    }

}

async function onSelectCategory() {

    hideCategories();

    if (available_categories.includes(this.id)) { // Получаем данные с сервера
        let category = new FormData;
        category.append('category', this.id);
        await fetchMaterialsList(category);

    } else {
        alert("Неверное имя категории");
        showCategories();
    }
}

function printMaterialRows(rowArray) {
    console.log(rowArray);
    for (let string in rowArray) {
        let row = document.createElement('p');
        row.className = 'row-text row';
        row.id = string;
        row.innerHTML = rowArray[string][0];
        localStorage.setItem('description of ' + rowArray[string][0], rowArray[string][1])
        menu.append(row);
    }
}

function selectEntry(e) {
    let target = e.target;

    if (target.classList.contains('row')) {
        writeIdToLocalStorage(target);
        renderMaterialName(target);
        renderMaterialDescription();
        renderMaterialLink(target);
        changeDataElements(target);
        showHideMenu();
    }
}

function writeIdToLocalStorage(target) {
    let id = target.id;
    localStorage.setItem('id', id);
    localStorage.setItem('name', target.innerHTML);
}

function getMaterialName() {
    return name = localStorage.getItem('name');
}
function getMaterialDescription() {
    name = getMaterialName();
    return(localStorage.getItem('description of ' + name));
}
function renderMaterialDescription() {
    materialDescription.innerHTML = getMaterialDescription();
}
function renderMaterialName(target) {
    materialName.innerHTML = target.innerHTML;
}

function renderMaterialLink(target) {
    materialLink.href = "https://yandex.ru/search/?text=" + target.innerHTML;
}