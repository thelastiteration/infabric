'use strict';

const physicalProps =
    {
        phys_id: 'ID',
        density: 'Плотность',
        endurance: 'Прочность',
        melting_point: 'Точка плавления',
        thermal_conduct: 'Теплопроводность',
        heat_capacity: 'Теплоёмкость',
        electrical_conduct: 'Электропроводность',
        magnetism: 'Магнитные свойства',
    };
const chemicalProps =
    {
        chem_id: 'ID',
        formula: 'Формула',
        mol_weight: 'Молекулярная масса',
        solubility: 'Растворимость в воде',
        crystallization: 'Температура кристаллизации',
        corrosion_resist: 'Сопротивление коррозии',
        chemical_activity: 'Химическая активность',
    };
const engineerProps =
    {
        chem_id: 'ID',
        plasticity: 'Пластичность',
        viscosity: 'Вязкость',
        cold_breakage: 'Ломкость',
        heat_resistance: 'Сопротивление нагреву',
        malleability: 'Пластичность',
        fluidity: 'Текучесть',
        weldability: 'Свариваемость',
    };
const geoProps =
    {
        chem_id: 'ID',
        mosh_scale: 'Шкала твёрдости Мооса',
    };

const physical = document.querySelector('#physical');
const chemical = document.querySelector('#chemical');
const engineering = document.querySelector('#engineering');
const geo = document.querySelector('#geological');

/**
 * Query the server to receive the dataset of materials
 *
 * @return {Promise<any>}
 */

async function queryDataFromServer() {
    let id = new FormData;
    let id_local = localStorage.getItem('id');
    id.append('id', id_local);
    let response = await fetch('php/MaterialData.php', {
        method: 'POST',
        body: id,
    });

    if (!response.ok) {
        alert('Произошла ошибка: ' + response.status);
    }
    return response.json();
}

/**
 * Changes data on main page blocks
 *
 * @param block element
 * @param response json
 * @param props object
 *
 * @return void
 * */

function renderBlockData(block, response, props) {

    if (response.length === 0) {
        response = 'Нет доступных данных.';
        let row = document.createElement('p');
        row.className = 'info-text data';
        row.innerHTML = response;
        block.append(row);
    }

    for (let line of Object.keys(response)) {

        let unit = '';
        let value = response[line];

        if (!(line in props)) {
            continue;
        }
        line = props[line];

        if (value == null) {
            continue;
        }
        if (line === "ID") {
            continue;
        }

        switch (line) {
            case 'Теплопроводность':
                unit = ' Ватт/м*К';
                break;
            case 'Точка плавления':
                unit = ' °C';
                break;
            case 'Температура кристаллизации':
                unit = ' °C';
                break;
            case 'Теплоёмкость':
                unit = ' Дж/(кг*K)';
                break;
            case 'Плотность':
                unit = ' г/см3';
                break;
            default:
                unit = '';
        }
        let row = document.createElement('p');
        row.className = 'info-text data';
        row.innerHTML = line + ': ' + value + unit;
        block.append(row);
    }
}

async function changeDataElements() {

    let response = await queryDataFromServer();
    console.log(response); // Для отладки, в проде удалить
    let clearingList = document.querySelectorAll('.data');

    for (let clear of clearingList) {
        clear.remove();
    }

    renderBlockData(physical, response, physicalProps);
    renderBlockData(chemical, response, chemicalProps);
    renderBlockData(engineering, response, engineerProps);
    renderBlockData(geo, response, geoProps);
}

async function loadGalleryImages() {
// asynchronous function body. to be created.
}