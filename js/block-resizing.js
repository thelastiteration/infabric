'use strict'

let blocks = ['#physical', '#chemical', '#engineering', '#geological'];

for (let blockTitle of blocks) {
    blockTitle = document.querySelector(blockTitle);
    blockTitle.addEventListener('click', blockResize);
}

function blockResize(e) {
    if (e.target.classList.contains('important')) {
        if (this.classList.contains('expanded')) {
            this.classList.remove('expanded');
        }
        else {
            this.classList.add('expanded');
        }

    }

}