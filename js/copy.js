'use strict'

// План переписывания кода:
// 1) Добавить слушатель на body.
// 2) Всплыванием зафиксировать действие копирования на тексте.
// 3) Записать в буфер обмена

let timer = setInterval(setTextSelectionListeners, 1000);

function setTextSelectionListeners() {
    let text = document.querySelectorAll(".info-text");
    for (let copy of text) {
        copy.addEventListener('click', Copy);
    }
}

function Copy() {
    // let i =
    navigator.clipboard.writeText(this.innerText);
    // let copyMessage = document.createElement('p');
    // copyMessage.className = 'copyMessage';
    // document.body.append(copyMessage);
}