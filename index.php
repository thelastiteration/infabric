<?php require_once "php/Database.php"; ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="scss/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;200;300;400;500;600;700;800;900&family=Raleway:wght@300;400&display=swap"
          rel="stylesheet">
    <script defer src="js/data-loading.js"></script>
    <script defer src="js/block-resizing.js"></script>
    <script defer src="js/menu.js"></script>
    <script defer src="js/copy.js"></script>
    <script defer src="js/about.js"></script>
    <title>Home Page</title>
</head>
<body class="page-structure">
<nav class="nav">
    <div class="logo">
        <p class="logo-text">Infabric</p>
        <p class="logo-text-additional">База знаний о материалах</p>
    </div>
    <div class="control">
        <a href="https://yandex.ru/search/?text=" target="_blank"
           class="material-link"><p class="material-name tooltip"><span class="tooltip-text">ОПИСАНИЕ</span>
                Материал:
                <strong>
                </strong></p></a>
        <button class="category-button button" onclick="showHideMenu()">Выбрать материал</button>
    </div>
</nav>
<div class="info-layer">
    <div class="info-block" id="physical"><p class="info-text important physical">Физические свойства</p>
    </div>
    <div class="info-block" id="chemical"><p class="info-text important chemical">Химические свойства</p>
    </div>
    <div class="info-block" id="engineering"><p class="info-text important engineering">Инженерные свойства</p>
    </div>
    <div class="info-block" id="geological"><p class="info-text important geological">Геологические свойства</p>
    </div>
</div>
<div class="menu hidden">
    <div class="submenu" id="minerals"><p class="submenu-text">Минералы</p></div>
    <div class="submenu" id="metals"><p class="submenu-text">Металлы</p></div>
    <div class="submenu" id="building"><p class="submenu-text">Строительство</p></div>
    <div class="submenu" id="medicine"><p class="submenu-text">Медицина</p></div>
    <div class="submenu" id="animals"><p class="submenu-text">Животноводство</p></div>
    <div class="submenu" id="engineering"><p class="submenu-text">Инженерия</p></div>
    <div class="submenu" id="fabrics"><p class="submenu-text">Текстиль</p></div>
    <div class="submenu" id="organics"><p class="submenu-text">Органика</p></div>
    <div class="submenu" id="armory"><p class="submenu-text">Военное дело</p></div>
    <div class="submenu" id="electronics"><p class="submenu-text">Электроника</p></div>
</div>
<div class="info-layer-2">
    <div class="info-block-2"><p class="info-text important">Изображения</p>
        <div class="gallery">
            <?php $id = $_COOKIE['id'] ?? 1;
            $path = "img/id/$id/" ?? '';
            if (!file_exists($path)) {
                die();
            }
            $fileNumber = new FilesystemIterator($path, FilesystemIterator::SKIP_DOTS);

            if (!empty($fileNumber)) {
                for ($i = 1; $i <= iterator_count($fileNumber); $i++) {
                    if ($i > 6) {
                        break;
                    }
                    echo "<a href='$path$i.jpg' target='_blank' class='photo-ref'><img src=$path$i.jpg class='photo' alt='photo'></a>";
                }
            }
            ?>
        </div>
    </div>
</div>
<footer class="copyright">
    <p class="copyright-text"> 2022 | Все права защищены. ©</p>
    <a class="link" href="" target="_blank">О проекте</a>
</footer>
</body>
</html>
