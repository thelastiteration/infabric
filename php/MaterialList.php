<?php // Получаем список материалов

require_once 'Validation.php';

class MaterialList // Класс, описывающий объект данных, представляющих из себя список материалов выбранной категории
{
    private string $category = "";
    private string $sql = ""; // Запрос к базе данных
    private array $data = [];

    public function __construct()
    {

        $this->setCategory();
        $this->setSQL();
        $this->sendFetchedData();
    }


    /**
     * Sets category of material for using in sql-query to database
     *
     * @return void
     */
    private function setCategory(): void
    {
        $validate = new Validation();
        // Берём переменную отправленную с фронта (POST) и присваиваем её свойству класса, предварительно фильтруя!
        if (isset($_POST['category'])) {
            $this->category = $validate->sanitizeCategory($_POST['category']);
        } else {
            $this->category = 'minerals';
        }
    }

    /**
     * Sets SQL-query string
     *
     * @return void
     */
    private function setSQL(): void
    {
        $this->sql = "SELECT * FROM infabric.materials WHERE category = '$this->category' LIMIT 10";
    }

    /**
     * Queries database and fetch list of materials of selected category
     *
     * @return array
     */
    private function fetchCategoryMaterials(): array
    {
        require_once 'Database.php';
        $sql = $this->sql;
        $data = $this->data;
        foreach ($pdo->query($sql) as $row) { // Создаём массив материалов нужной категории, взятых из базы данных
            $data[$row['id']] = array($row['name'], $row['description']);
        }
        return $data;
    }

    /**
     * Send fetched from db data to the frontend
     *
     * @return void
     */
    private function sendFetchedData(): void
    {
        $data = $this->fetchCategoryMaterials();
        echo json_encode($data); // Возвращаем json на фронтенд
    }
}

$category = new MaterialList;