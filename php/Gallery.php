<?php // Реализация загрузки картинок в галерею

class Gallery
{
    private string $id = '';
    private string $path = '';


    public function __construct()
    {
        $this->setId();
        $this->setPath();
        $this->loadImage();

    }

    private function setId(): void
    {
        $this->id = $_COOKIE['id'];
    }

    private function setPath(): void
    {
        $this->path = "img/id/$this->id/";
    }

    private function loadImage(): void
    {
        $path = $this->path;
        for ($i = 1; $i <= iterator_count($this->getImagesCount()); $i++) {
            echo "<a href='$path$i.jpg' target='_blank' class='photo-ref'><img src=$path$i.jpg class='photo' alt='photo'></a>";
        }
    }

    private function getImagesCount(): FilesystemIterator
    {
        $path = $this->path;
        return new FilesystemIterator($path, FilesystemIterator::SKIP_DOTS);

    }
}

$gallery = new Gallery;
