<?php // Получаем данные о выбранном материале

require_once 'Validation.php';

class MaterialData
{
    private int $id = 1;
    private array $result = [];

    public function __construct()
    {
        $this->validateID();
        $this->sendFetchedData();
    }

    /**
     * Validate ID got from frontend
     *
     * @return void
     */
    private function validateID(): void
    {
        require_once 'Validation.php';
        $validate = new Validation();

        if (isset($_POST['id'])) {
            $this->id = $validate->sanitizeID($_POST['id']);
        } else {
            $this->id = 1;
        }
    }

    /**
     * Fetch all the data related to the selected material
     *
     * @return array|string[]
     */
    private function fetchMaterialData(): array
    {
        require_once 'Database.php';
        $id = $this->id;
        $result = $this->result;
        $sql = $pdo->prepare("SELECT * FROM physical INNER JOIN chemical
ON physical.phys_id=chemical.chem_id INNER JOIN engineering ON engee_id=phys_id INNER JOIN geological ON geo_id=phys_id
WHERE phys_id = :id");
        $sql->bindParam(':id', $id);
        $sql->execute();
        $data = $sql->fetch(PDO::FETCH_ASSOC);

        foreach ($data as $row => $value) {
            $result[$row] = $value;
        }

        if (!empty($result)) {
            return $result;
        } else {
            return [];
        }
    }

    /**
     * Send fetched data to the frontend
     *
     * @return void
     */
    private function sendFetchedData(): void
    {
        $data = $this->fetchMaterialData();
        echo json_encode($data);
    }
}

$materialData = new MaterialData;